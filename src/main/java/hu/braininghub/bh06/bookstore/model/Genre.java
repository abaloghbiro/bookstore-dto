package hu.braininghub.bh06.bookstore.model;

public enum Genre {

	DRAMA, SCI_FI, CRIME, COMEDY;
}

package hu.braininghub.bh06.bookstore.model;

public class Book {

	private String title;
	private String author;
	private String isbn;
	private int year;
	private Genre genre;
	private Double price;
	private Integer availableNum;

	public Book(String title, String author, String isbn, int year, Genre genre, Double price) {

		this.title = title;
		this.author = author;
		this.isbn = isbn;
		this.year = year;
		this.genre = genre;
		this.price = price;
		this.availableNum = 0;
	}

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}

	public String getIsbn() {
		return isbn;
	}

	public int getYear() {
		return year;
	}

	public Genre getGenre() {
		return genre;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getAvailableNum() {
		return availableNum;
	}

	public void setAvailableNum(Integer availableNum) {
		this.availableNum = availableNum;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((isbn == null) ? 0 : isbn.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (isbn == null) {
			if (other.isbn != null)
				return false;
		} else if (!isbn.equals(other.isbn))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Book [title=" + title + ", author=" + author + ", isbn=" + isbn + ", year=" + year + ", genre=" + genre
				+ ", price=" + price + ", availableNum=" + availableNum + "]";
	}

}

package hu.braininghub.bh06.bookstore.service;

public class BookNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BookNotFoundException(String isbn) {
		super("Book not found with the given attribute: " + isbn);
	}
}

package hu.braininghub.bh06.bookstore.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hu.braininghub.bh06.bookstore.dao.BookDAO;
import hu.braininghub.bh06.bookstore.dto.BookDTO;
import hu.braininghub.bh06.bookstore.event.Event;
import hu.braininghub.bh06.bookstore.event.Notifiable;
import hu.braininghub.bh06.bookstore.model.Book;

public class DefaultBookService implements BookService, Notifiable {

	private BookDAO dao;

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultBookService.class);

	public DefaultBookService(BookDAO dao) {
		this.dao = dao;
	}

	public void buyBook(BookDTO b) {

		LOGGER.info("In buyBook method -- New book is {}", b);

		Book book = dao.getBookByIsbn(b.getIsbn());

		if (book == null) {
			Book newBook = new Book(b.getTitle(), b.getAuthor(), b.getIsbn(), b.getYear(), b.getGenre(), b.getPrice());
			newBook.setAvailableNum(b.getAvailableNum());
			dao.saveBook(newBook);
		} else {
			throw new IllegalArgumentException("Book exists with the specified isbn!");
		}

	}

	public BookDTO sellBook(String isbn) throws BookNotFoundException {

		Book book = dao.getBookByIsbn(isbn);
		BookDTO ret = null;

		if (book != null && book.getAvailableNum() > 0) {
			book.setAvailableNum(book.getAvailableNum() - 1);
			ret = new BookDTO(book.getTitle(), book.getAuthor(), book.getIsbn(), book.getYear(), book.getGenre(),
					book.getPrice());
			dao.updateBook(book);
		} else {
			throw new BookNotFoundException(isbn);
		}

		return ret;
	}

	public List<BookDTO> getBooks() {

		LOGGER.info("In getBooks method...");

		List<BookDTO> ret = new ArrayList<BookDTO>();

		dao.getBooks().stream().forEach(book -> ret.add(new BookDTO(book.getTitle(), book.getAuthor(), book.getIsbn(),
				book.getYear(), book.getGenre(), book.getPrice())));

		return ret;
	}

	@Override
	public void receiveNotification(Event e) {
		System.out.println(e);
	}

}

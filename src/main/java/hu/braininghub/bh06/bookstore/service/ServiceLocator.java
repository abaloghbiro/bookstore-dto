package hu.braininghub.bh06.bookstore.service;

import hu.braininghub.bh06.bookstore.dao.DefaultBookDAO;

public class ServiceLocator {

	private static BookService bookService;

	public static BookService getBookService() {

		if (bookService == null) {
			bookService = new DefaultBookService(new DefaultBookDAO());
		}
		return bookService;
	}
}

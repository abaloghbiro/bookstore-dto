package hu.braininghub.bh06.bookstore.service;

import java.util.List;

import hu.braininghub.bh06.bookstore.dto.BookDTO;

public interface BookService {

	void buyBook(BookDTO b);
	
	BookDTO sellBook(String isbn) throws BookNotFoundException;
	
	List<BookDTO> getBooks();
}

package hu.braininghub.bh06.bookstore.dao;

import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import hu.braininghub.bh06.bookstore.model.Book;

public class JdbcBookDAO implements BookDAO {

	private JdbcTemplate template = new JdbcTemplate();

	public JdbcBookDAO() throws Exception {
		Class.forName("oracle.jdbc.driver.OracleDriver");
		DataSource ds = new DriverManagerDataSource("jdbc:oracle:thin:@localhost:1521:xe", "system", "admin123");
		template.setDataSource(ds);
	}

	public Book getBookByTitle(String title) {
		return template.queryForObject("SELECT * FROM BOOK WHERE TITLE = ?", new Object[] { title }, Book.class);
	}

	public void saveBook(Book book) {
		template.update(
				"INSERT INTO BOOK (ISBN,AUTHOR,TITLE,YEAR,GENRE,PRICE,AVAILABLE_NUM)\" + \"VALUES (?,?,?,?,?,?,?)",
				book.getIsbn(), book.getAuthor(), book.getTitle(), book.getYear(), book.getGenre(), book.getPrice(),
				book.getAvailableNum());
	}

	public void deleteBook(String isbn) {
		template.update("DELETE FROM BOOK WHERE ISBN = ?", isbn);
	}

	public Book getBookByIsbn(String isbn) {
		return template.queryForObject("SELECT * FROM BOOK WHERE ISBN = ?", new Object[] { isbn }, Book.class);
	}

	public List<Book> getBooks() {
		return template.queryForList("SELECT * FROM BOOK ", Book.class);
	}

	@Override
	public void updateBook(Book book) {

	}
}
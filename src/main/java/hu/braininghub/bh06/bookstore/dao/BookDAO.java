package hu.braininghub.bh06.bookstore.dao;

import java.util.List;

import hu.braininghub.bh06.bookstore.model.Book;

public interface BookDAO {

	Book getBookByTitle(String title);
	void saveBook(Book book);
	void updateBook(Book book);
	void deleteBook(String isbn);
	Book getBookByIsbn(String isbn);
	List<Book> getBooks();
}

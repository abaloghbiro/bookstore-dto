package hu.braininghub.bh06.bookstore.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import hu.braininghub.bh06.bookstore.model.Book;
import hu.braininghub.bh06.bookstore.model.Genre;

public class DefaultBookDAO implements BookDAO {

	private List<Book> books = new ArrayList<Book>();

	public DefaultBookDAO() {
		
		books.add(new Book("test1", "author1", "isbn1", 2010, Genre.COMEDY, 2120.2));
		books.add(new Book("test2", "author2", "isbn2", 2011, Genre.CRIME, 2121.2));
		books.add(new Book("test3", "author3", "isbn3", 2012, Genre.SCI_FI, 2122.2));
		books.add(new Book("test4", "author4", "isbn4", 2013, Genre.COMEDY, 2123.2));
		books.add(new Book("test5", "author5", "isbn5", 2014, Genre.DRAMA, 2124.2));
	}

	public Book getBookByTitle(String title) {

		Book ret = null;

		for (Book b : books) {

			if (title.equals(b.getTitle())) {
				ret = b;
				break;
			}
		}
		return ret;
	}

	public void saveBook(Book book) {
		books.add(book);
	}

	public void deleteBook(String isbn) {

		Iterator<Book> it = books.iterator();

		while (it.hasNext()) {

			Book b = it.next();

			if (b.getIsbn().equals(isbn)) {
				it.remove();
			}
		}

	}

	public Book getBookByIsbn(String isbn) {
		Book ret = null;

		for (Book b : books) {

			if (isbn.equals(b.getIsbn())) {
				ret = b;
				break;
			}
		}
		return ret;
	}

	public List<Book> getBooks() {
		return Collections.unmodifiableList(books);
	}

	@Override
	public void updateBook(Book book) {

		Book old = getBookByIsbn(book.getIsbn());
		books.remove(old);
		books.add(book);

	}

}

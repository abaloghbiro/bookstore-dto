package hu.braininghub.bh06.bookstore.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hu.braininghub.bh06.bookstore.event.Event;
import hu.braininghub.bh06.bookstore.event.QueueBasedEventBus;

public class GenericMapModel implements Model {

	private Map<String, Object> map = new HashMap<>();
	
	public static final String BOOKS_ATTRIBUTE_NAME = "books";
	public static final String ERROR_MESSAGE_ATTRIBUTE_NAME = "errorMessage";

	@Override
	public void addAttribute(String attributeName, Object value) {
		map.put(attributeName, value);
		Event<String> e = new ModelChangedEvent(this, new Date(), attributeName);
		QueueBasedEventBus.INSTANCE.sendMessage(e);

	}

	@Override
	public void removeAttribute(String name) {
		map.remove(name);
		Event<String> e = new ModelChangedEvent(this, new Date(), name);
		QueueBasedEventBus.INSTANCE.sendMessage(e);
	}

	@Override
	public Object getAttribute(String name) {
		return map.get(name);
	}

	@Override
	public List<Object> getAllAttributes() {
		return new ArrayList<>(map.values());
	}

}

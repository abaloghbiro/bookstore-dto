package hu.braininghub.bh06.bookstore.ui;

import java.util.List;

public interface Model {

	void addAttribute(String attributeName, Object value);

	void removeAttribute(String name);

	Object getAttribute(String name);

	List<Object> getAllAttributes();

}

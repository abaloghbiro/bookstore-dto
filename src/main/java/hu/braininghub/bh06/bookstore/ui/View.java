package hu.braininghub.bh06.bookstore.ui;

import hu.braininghub.bh06.bookstore.event.Notifiable;

public interface View extends Notifiable {

	void initView();
	
	void refreshView();
}

package hu.braininghub.bh06.bookstore.ui.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;

import hu.braininghub.bh06.bookstore.dto.BookDTO;
import hu.braininghub.bh06.bookstore.event.Event;
import hu.braininghub.bh06.bookstore.event.QueueBasedEventBus;
import hu.braininghub.bh06.bookstore.model.Genre;
import hu.braininghub.bh06.bookstore.ui.BookstoreController;
import hu.braininghub.bh06.bookstore.ui.View;

public class BookstoreEditorView extends JDialog implements ActionListener, View {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField jTextfieldTitle;
	private JTextField jtextFieldAuthor;
	private JTextField jtextFieldIsbn;
	private JTextField jtextFieldAvailableNum;
	private JComboBox<Integer> jComboBoxYear;
	private JComboBox<Genre> jComboBoxGenre;
	private Component parent;
	private String queueAddress;

	private BookstoreController controller;
	private JTextField textField;

	public BookstoreEditorView(Component parent, BookstoreController controller) {
		this.controller = controller;
		this.parent = parent;
		queueAddress = QueueBasedEventBus.INSTANCE.subScribe(this);
		System.out.println("QUEUE ADDRESS IS : " + queueAddress);
		initView();
	}

	@Override
	public void initView() {

		setModal(true);
		setUndecorated(true);

		Dimension d = new Dimension(500, 620);
		setMinimumSize(d);

		this.setLocationRelativeTo(parent);

		setTitle("Create new book");
		getContentPane().setLayout(null);

		jTextfieldTitle = new JTextField();
		jTextfieldTitle.setBounds(198, 80, 206, 35);
		getContentPane().add(jTextfieldTitle);
		jTextfieldTitle.setColumns(10);

		jtextFieldAuthor = new JTextField();
		jtextFieldAuthor.setBounds(198, 139, 206, 35);
		getContentPane().add(jtextFieldAuthor);
		jtextFieldAuthor.setColumns(10);

		jtextFieldIsbn = new JTextField();
		jtextFieldIsbn.setBounds(198, 198, 206, 35);
		getContentPane().add(jtextFieldIsbn);
		jtextFieldIsbn.setColumns(10);

		JLabel jLabelTitle = new JLabel("Title");
		jLabelTitle.setBounds(37, 80, 104, 29);
		getContentPane().add(jLabelTitle);

		JLabel jLabelAuthor = new JLabel("Author");
		jLabelAuthor.setBounds(37, 139, 104, 29);
		getContentPane().add(jLabelAuthor);

		JLabel jLabelIsbn = new JLabel("ISBN");
		jLabelIsbn.setBounds(37, 198, 104, 29);
		getContentPane().add(jLabelIsbn);

		JLabel jLabelYear = new JLabel("Year");
		jLabelYear.setBounds(37, 257, 104, 29);
		getContentPane().add(jLabelYear);

		JLabel jLabelGenre = new JLabel("Genre");
		jLabelGenre.setBounds(37, 316, 104, 29);
		getContentPane().add(jLabelGenre);

		JLabel jLabelAvailableNum = new JLabel("Available num");
		jLabelAvailableNum.setBounds(37, 425, 104, 29);
		getContentPane().add(jLabelAvailableNum);

		Integer[] years = new Integer[119];

		for (int i = 0; i <= 118; i++) {
			years[i] = 1900 + i;
		}

		ComboBoxModel<Integer> yearModel = new DefaultComboBoxModel<>(years);
		jComboBoxYear = new JComboBox<>();
		jComboBoxYear.setModel(yearModel);
		jComboBoxYear.setBounds(198, 257, 206, 35);
		getContentPane().add(jComboBoxYear);

		jComboBoxGenre = new JComboBox<>();
		ComboBoxModel<Genre> genreModel = new DefaultComboBoxModel<>(Genre.values());
		jComboBoxGenre.setBounds(198, 313, 206, 35);
		jComboBoxGenre.setModel(genreModel);
		getContentPane().add(jComboBoxGenre);

		jtextFieldAvailableNum = new JTextField();

		jtextFieldAvailableNum.setBounds(198, 422, 206, 35);
		getContentPane().add(jtextFieldAvailableNum);

		JButton button = new JButton("Save");
		button.addActionListener(this);
		button.setBounds(48, 496, 155, 37);
		getContentPane().add(button);

		JButton jButtonCancel = new JButton("Cancel");
		jButtonCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				QueueBasedEventBus.INSTANCE.unSubscribe(queueAddress);
				setVisible(false);
			}
		});
		jButtonCancel.setBounds(225, 496, 155, 37);
		getContentPane().add(jButtonCancel);

		textField = new JTextField();
		textField.setBounds(198, 366, 206, 35);
		getContentPane().add(textField);

		JLabel label = new JLabel("Price");
		label.setBounds(37, 369, 104, 29);
		getContentPane().add(label);

		setVisible(true);

	}

	@Override
	public void refreshView() {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		BookDTO newBook = new BookDTO(jTextfieldTitle.getText(), jtextFieldAuthor.getText(), jtextFieldIsbn.getText(),
				(Integer) jComboBoxYear.getSelectedItem(), (Genre) jComboBoxGenre.getSelectedItem(),
				Double.parseDouble(jtextFieldAvailableNum.getText()));
		newBook.setAvailableNum(Integer.parseInt(jtextFieldAvailableNum.getText()));

		controller.buyBook(newBook);
		QueueBasedEventBus.INSTANCE.unSubscribe(queueAddress);
		this.setVisible(false);
	}

	@Override
	public void receiveNotification(Event e) {
		refreshView();
	}
}

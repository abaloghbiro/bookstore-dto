package hu.braininghub.bh06.bookstore.ui;

import java.util.Date;

import hu.braininghub.bh06.bookstore.event.TextBasedEvent;

public class ModelChangedEvent extends TextBasedEvent{

	public ModelChangedEvent(Object source, Date timestamp,String payload) {
		super(source, timestamp, payload);
	}

	@Override
	public String toString() {
		return "ModelChangedEvent [getSource()=" + getSource() + ", getTimestamp()=" + getTimestamp()
				+ ", getPayload()=" + getPayload() + ", toString()=" + super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}
	
	

}

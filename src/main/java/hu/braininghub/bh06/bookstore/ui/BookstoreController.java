package hu.braininghub.bh06.bookstore.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import hu.braininghub.bh06.bookstore.dao.JdbcBookDAO;
import hu.braininghub.bh06.bookstore.dto.BookDTO;
import hu.braininghub.bh06.bookstore.service.BookService;
import hu.braininghub.bh06.bookstore.service.DefaultBookService;

public class BookstoreController {

	private GenericMapModel model;

	private BookService bookService;

	public BookstoreController() throws Exception {
		this.model = new GenericMapModel();
		this.bookService = new DefaultBookService(new JdbcBookDAO());
		model.addAttribute(GenericMapModel.BOOKS_ATTRIBUTE_NAME, bookService.getBooks());
	}

	public GenericMapModel getModel() {
		return model;
	}

	@SuppressWarnings("unchecked")
	public void saveBooksToFileSystem() throws IOException {

		List<BookDTO> books = (List<BookDTO>) model.getAttribute(GenericMapModel.BOOKS_ATTRIBUTE_NAME);

		File file = new File(System.getProperty("user.home") + File.separator + "books.dat");

		// if file doesnt exists, then create it
		if (!file.exists()) {
			file.createNewFile();
		}

		FileOutputStream fos = new FileOutputStream(file);
		ObjectOutputStream oos = new ObjectOutputStream(fos);

		oos.writeObject(books);
		oos.flush();
		oos.close();
	}

	@SuppressWarnings({ "unchecked" })
	public void openBooks(File datFile) throws Exception {

		// if file doesnt exists, then create it
		if (!datFile.exists()) {
			throw new IllegalArgumentException("Missing DAT file!");
		}

		FileInputStream fis = new FileInputStream(datFile);
		ObjectInputStream oos = new ObjectInputStream(fis);

		List<BookDTO> books = (List<BookDTO>) oos.readObject();

		model.removeAttribute(GenericMapModel.BOOKS_ATTRIBUTE_NAME);
		model.addAttribute(GenericMapModel.BOOKS_ATTRIBUTE_NAME, books);

		oos.close();
	}

	public void buyBook(BookDTO dto) {
		nonNullValidator(dto);
		stringValidator(dto.getAuthor());
		stringValidator(dto.getTitle());
		stringValidator(dto.getIsbn());

		if (dto.getAvailableNum() <= 0) {
			throw new IllegalArgumentException("Mimimum available num should be at least 1!");
		}
		bookService.buyBook(dto);

		List<BookDTO> books = bookService.getBooks();
		model.addAttribute(GenericMapModel.BOOKS_ATTRIBUTE_NAME, books);
	}

	public void sellBook(String isbn) {
		bookService.sellBook(isbn);
		List<BookDTO> books = bookService.getBooks();
		model.addAttribute(GenericMapModel.BOOKS_ATTRIBUTE_NAME, books);
	}

	private void nonNullValidator(Object param) {

		if (param == null) {
			throw new IllegalArgumentException("This object cannot be null!");
		}
	}

	private void stringValidator(String param) {

		nonNullValidator(param);

		if (param.isEmpty()) {
			throw new IllegalArgumentException("This String cannot be empty!");
		}
	}
}

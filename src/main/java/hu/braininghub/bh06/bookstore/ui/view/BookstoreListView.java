package hu.braininghub.bh06.bookstore.ui.view;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hu.braininghub.bh06.bookstore.dto.BookDTO;
import hu.braininghub.bh06.bookstore.event.Event;
import hu.braininghub.bh06.bookstore.event.QueueBasedEventBus;
import hu.braininghub.bh06.bookstore.ui.BookstoreController;
import hu.braininghub.bh06.bookstore.ui.GenericMapModel;
import hu.braininghub.bh06.bookstore.ui.ModelChangedEvent;
import hu.braininghub.bh06.bookstore.ui.View;

public class BookstoreListView extends JFrame implements View, ActionListener {

	private static final long serialVersionUID = 1L;
	private BookstoreController controller;
	private JTable jTableBookstore;
	private Logger logger = LoggerFactory.getLogger(BookstoreListView.class);
	private String queueAddress;

	public BookstoreListView(BookstoreController controller) {
		this.controller = controller;
		initView();
		queueAddress = QueueBasedEventBus.INSTANCE.subScribe(this);
		System.out.println("QUEUE ADDRESS IS : " + queueAddress);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void initView() {

		JMenuBar jMainMenubar = new JMenuBar();
		setJMenuBar(jMainMenubar);

		JMenu jMenuFile = new JMenu("F\u00E1jl");
		jMainMenubar.add(jMenuFile);

		Component actualFrame = this;
		JMenuItem jMenuItemNewBookCreation = new JMenuItem("\u00DAj k\u00F6nyv l\u00E9trehoz\u00E1sa");
		jMenuItemNewBookCreation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new BookstoreEditorView(actualFrame, controller);
			}
		});
		jMenuFile.add(jMenuItemNewBookCreation);

		JMenuItem jMenuItemExit = new JMenuItem("Kil\u00E9p\u00E9s");
		jMenuItemExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				QueueBasedEventBus.INSTANCE.unSubscribe(queueAddress);
				System.exit(0);
			}
		});

		JMenuItem jMenuItemSave = new JMenuItem("Ment\u00E9s");
		jMenuItemSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					controller.saveBooksToFileSystem();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		JMenuItem jMenuItemOpen = new JMenuItem("Megnyit\u00E1s");

		Component owner = this;

		jMenuItemOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {

					JFileChooser fileChooser = new JFileChooser();
					fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
					fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
					fileChooser.addChoosableFileFilter(new FileFilter() {

						public String getDescription() {
							return "DAT Files (*.dat)";
						}

						public boolean accept(File f) {
							if (f.isDirectory()) {
								return true;
							} else {
								return f.getName().toLowerCase().endsWith(".dat");
							}
						}
					});
					fileChooser.setAcceptAllFileFilterUsed(true);

					int result = fileChooser.showOpenDialog(owner);

					if (result == JFileChooser.APPROVE_OPTION) {
						File selectedFile = fileChooser.getSelectedFile();
						System.out.println("Selected file: " + selectedFile.getAbsolutePath());
						controller.openBooks(selectedFile);
					} else {
						JOptionPane.showMessageDialog(owner, "Please select a file if you want...");
					}

				} catch (Exception e1) {
					logger.error("Error occured: " + e1);
					e1.printStackTrace();
				}
			}
		});
		jMenuFile.add(jMenuItemOpen);
		jMenuFile.add(jMenuItemSave);
		jMenuFile.add(jMenuItemExit);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		getContentPane().setLayout(gridBagLayout);

		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 0;
		getContentPane().add(scrollPane, gbc_scrollPane);

		List<BookDTO> books = (List<BookDTO>) controller.getModel().getAttribute(GenericMapModel.BOOKS_ATTRIBUTE_NAME);

		BookStoreTableModel m = new BookStoreTableModel(books);
		jTableBookstore = new JTable(m) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public Class<?> getColumnClass(int column) {
				return getValueAt(0, column).getClass();
			}
		};
		jTableBookstore.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		new ButtonColumn(jTableBookstore, 6, this);
		scrollPane.setViewportView(jTableBookstore);
		pack();
		setVisible(true);

	}

	@SuppressWarnings("unchecked")
	@Override
	public void refreshView() {
		List<BookDTO> books = (List<BookDTO>) controller.getModel().getAttribute(GenericMapModel.BOOKS_ATTRIBUTE_NAME);
		jTableBookstore.setModel(new BookStoreTableModel(books));
		new ButtonColumn(jTableBookstore, 6, this);
		DefaultTableModel m = (DefaultTableModel) jTableBookstore.getModel();
		m.fireTableDataChanged();
	}

	@Override
	public void receiveNotification(Event e) {

		if (e instanceof ModelChangedEvent) {
			refreshView();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JOptionPane.showConfirmDialog(this, "Do you want to sell this book?"+e.getActionCommand());
	}
}

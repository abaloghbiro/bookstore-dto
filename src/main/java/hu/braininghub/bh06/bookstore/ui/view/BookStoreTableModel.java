package hu.braininghub.bh06.bookstore.ui.view;

import java.util.List;

import javax.swing.table.DefaultTableModel;

import hu.braininghub.bh06.bookstore.dto.BookDTO;

public class BookStoreTableModel extends DefaultTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BookStoreTableModel(List<BookDTO> books) {
		super(mapData(books), new String[] { "Title", "Author", "Isbn", "Year", "Genre", "Price", "" });
	}

	private static Object[][] mapData(List<BookDTO> books) {

		Object[][] data = new Object[books.size()][7];

		for (int rownum = 0; rownum < books.size(); rownum++) {
			data[rownum][0] = books.get(rownum).getTitle();
			data[rownum][1] = books.get(rownum).getAuthor();
			data[rownum][2] = books.get(rownum).getIsbn();
			data[rownum][3] = books.get(rownum).getYear();
			data[rownum][4] = books.get(rownum).getGenre().name();
			data[rownum][5] = books.get(rownum).getPrice();
			data[rownum][6] = "Sale";
		}

		return data;
	}
}

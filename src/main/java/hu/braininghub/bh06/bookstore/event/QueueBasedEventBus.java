package hu.braininghub.bh06.bookstore.event;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;

public class QueueBasedEventBus implements EventBus {

	private static final Map<String, Notifiable> OBSERVERS = new HashMap<>();

	private static final Queue<Doublet<String, Event<?>>> MESSAGES = new LinkedBlockingQueue<>();

	public static final EventBus INSTANCE = new QueueBasedEventBus();

	private Timer timer;

	private QueueBasedEventBus() {
		timer = new Timer(true);
		timer.scheduleAtFixedRate(new MessageSenderTask(), 0, 1000);
	}

	@Override
	public String subScribe(Notifiable n) {

		String address = UUID.randomUUID().toString();

		if (OBSERVERS.get(address) == null) {
			OBSERVERS.put(address, n);
		} else {
			throw new IllegalStateException("Observer with name already registered!");
		}

		return address;

	}

	@Override
	public void unSubscribe(String address) {
		OBSERVERS.remove(address);
	}

	@Override
	public void sendMessage(String address, Event<?> m) {

		Doublet<String, Event<?>> d = new Doublet<>();
		d.setKey(address);
		d.setValue(m);

		MESSAGES.offer(d);
	}

	@Override
	public void sendMessage(Event<?> m) {
		Doublet<String, Event<?>> d = new Doublet<>();
		d.setValue(m);
		MESSAGES.offer(d);

	}

	private static final class Doublet<K, V> {

		private K key;
		private V value;

		public K getKey() {
			return key;
		}

		public void setKey(K key) {
			this.key = key;
		}

		public V getValue() {
			return value;
		}

		public void setValue(V value) {
			this.value = value;
		}
	}

	private static class MessageSenderTask extends TimerTask {

		@Override
		public void run() {

			Doublet<String, Event<?>> queueEntry = MESSAGES.poll();

			if (queueEntry != null) {

				System.out.println("Event processing: " + queueEntry.getValue());

				if (queueEntry.getKey() != null) {

					Notifiable receiver = OBSERVERS.get(queueEntry.getKey());

					receiver.receiveNotification(queueEntry.getValue());
				} else {
					OBSERVERS.values().forEach(o -> o.receiveNotification(queueEntry.getValue()));
				}
			}

		}

	}

}

package hu.braininghub.bh06.bookstore.event;

public interface Notifiable {

	void receiveNotification(Event<?> e);
}

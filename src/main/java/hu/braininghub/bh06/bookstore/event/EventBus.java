package hu.braininghub.bh06.bookstore.event;

public interface EventBus {

	String subScribe(Notifiable n);
	
	void unSubscribe(String address);
	
	void sendMessage(String address, Event<?> m);
	
	void sendMessage(Event<?> m);
}

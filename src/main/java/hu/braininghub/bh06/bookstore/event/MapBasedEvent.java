package hu.braininghub.bh06.bookstore.event;

import java.util.Date;
import java.util.Map;

public class MapBasedEvent extends Event<Map<String, Object>> {

	public MapBasedEvent(Object source, Date timestamp, Map<String, Object> map) {
		super(source, timestamp, map);
	}

	public void addAttribute(String name, Object value) {
		this.getPayload().put(name, value);
	}

}

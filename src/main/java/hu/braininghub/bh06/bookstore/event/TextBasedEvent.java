package hu.braininghub.bh06.bookstore.event;

import java.util.Date;

public class TextBasedEvent extends Event<String> {

	public TextBasedEvent(Object source, Date timestamp, String username) {
		super(source, timestamp, username);
	}

}

package hu.braininghub.bh06.bookstore.app;

import hu.braininghub.bh06.bookstore.ui.BookstoreController;
import hu.braininghub.bh06.bookstore.ui.view.BookstoreListView;

public class App {

	public static void main(String[] args) throws Exception {
		
		BookstoreController c = new BookstoreController();
		
		new BookstoreListView(c);

	}

}
